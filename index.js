module.exports=function(crawl) {
	let delay=1000;

	if (crawl.inputs.delay)
		delay=crawl.inputs.delay;

	let i=0;
	let interval=setInterval(()=>{
		i++;

		crawl.result("hello "+crawl.inputs.name+" on line "+i);
		crawl.progress(100*i/10);

		if (i==5) {
			crawl.log("Mid crawl message!");
			crawl.log("Also on a second line...");
		}

		if (i==10) {
			clearInterval(interval);

			if (crawl.inputs.shouldFail && crawl.inputs.shouldFail!="no") {
				crawl.error("Error in crawler: "+crawl.inputs.shouldFail);
//				throw new Error("Error in crawler: "+crawl.inputs.shouldFail);
			}

			else {
				crawl.done();
			}
		}
	},delay);
};